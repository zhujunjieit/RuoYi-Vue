package com.ruoyi.quartz.task;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.common.utils.sign.Base64;
import com.ruoyi.common.utils.sign.Md5Utils;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }

    /**
     * 同步考勤数据
     */
    public void syncAttendanceData() throws Exception {
    }

    public static void main(String[] args) throws Exception {
        System.out.println("中文");
        String oaToken = RyTask.getOaToken();
        JSONArray oaMeeting = RyTask.getOaMeeting(oaToken);

    }
    public static String syncMeetingRoomList() throws Exception{
        return "";
    }
    public static String syncMeetingList() throws Exception{
        String oaToken = RyTask.getOaToken();
        JSONArray oaMeetingArr = RyTask.getOaMeeting(oaToken);

        String maxhubToken = RyTask.getMaxhubToken();
        RyTask.postMeeting(maxhubToken);
        return "";
    }


    public final static String OA_TOKEN_URL = "http://oa.zhgxfiber.com:8010/eoffice/server/public/api/open-api/get-token";
    public final static String OA_APP_ID = "100002";
    public final static String OA_App_Secret = "jozoNWZTu5H2ACU64dDOqjgJNGLlGzKE";

    public static String getOaToken() throws Exception{
        String token = "";

        JSONObject bodyMap = new JSONObject();
        bodyMap.put("agent_id", OA_APP_ID);
        bodyMap.put("secret",OA_App_Secret);
        bodyMap.put("user", "admin");
        String body = HttpUtil.createPost(RyTask.OA_TOKEN_URL).body(bodyMap.toString()).execute().body();
        JSONObject jsonObject = JSONObject.parseObject(body);
        if(jsonObject.getString("status").equals("1")){
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            token = jsonObject1.getString("token");
        }else{
            throw new Exception("获取OA的token失败；"+body);
        }
        return token;
    }
    public final static String OA_MEETING_LIST = "http://oa.zhgxfiber.com:8010/eoffice/server/public/api/meeting/get-all-meeting-list?start=%s&end=%s&order_by={\"meeting_apply_id\":\"desc\"}";
    public static JSONArray getOaMeeting(String oaToken) throws Exception{
        String startTime = DateUtil.now();
        String endTime= DateUtil.formatDateTime(DateUtil.endOfDay(DateUtil.date()));
        String oaMeetingList = String.format(OA_MEETING_LIST, startTime, endTime);
        String body = HttpUtil.createGet(oaMeetingList).bearerAuth(oaToken)
                .execute().body();
        JSONObject jsonObject = JSONObject.parseObject(body);
        JSONArray list ;
        if(jsonObject.getString("status").equals("1")){
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            list = jsonObject1.getJSONArray("list");
        }else{
            throw new Exception("获取OA的所有会议失败；"+body);
        }
        return list;
    }

    //会议室同步
    public final static String OA_MEETING_ROOM_LIST = "http://oa.zhgxfiber.com:8010/eoffice/server/public/api/meeting/meeting-room/listRoom";
    public static JSONArray getOaMeetingRoomList(String oaToken) throws Exception{
        String body = HttpUtil.createGet(OA_MEETING_ROOM_LIST).bearerAuth(oaToken)
                .execute().body();
        JSONObject jsonObject = JSONObject.parseObject(body);
        JSONArray list ;
        if(jsonObject.getString("status").equals("1")){
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            list = jsonObject1.getJSONArray("list");
        }else{
            throw new Exception("获取OA的所有会议失败；"+body);
        }
        return list;
    }
    /**
     * --------------------------------------maxhub会议接口------------------------------------------
     */

    public final static String MAXHUB_TOKEN_URL = "https://gateway.maxhub.com/palm/api/auth/token?grant_type=client_credentials";
    public final static String MAXHUB_APP_ID = "f643e109-52f4-4f3c-ad01-25f07b37a1ec";
    public final static String MAXHUB_APP_SECRET = "FUC3COuqvr1Zk4d8";

    public static String getMaxhubToken() throws Exception{
        String token = "";
        String body = HttpUtil.createPost(RyTask.MAXHUB_TOKEN_URL).basicAuth(MAXHUB_APP_ID,MAXHUB_APP_SECRET).execute().body();
        JSONObject jsonObject = JSONObject.parseObject(body);
        if(jsonObject.getString("code").equals("201")){
            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            token = jsonObject1.getString("access_token");
        }else{
            throw new Exception("获取Maxhub的Toeken失败；"+body);
        }
        return token;
    }
    /**
     * 判断会议是否存在
     * @param maxhubToken
     * @param refId
     * @return
     * @throws Exception
     */
    public final static String GET_MEETING_URL = "https://gateway.maxhub.com/meeting/api/v1/client/meetings/{meeting_no}";
    public static boolean getMaxhubMeeting(String maxhubToken,String meetingNo) throws Exception{

        return true;
    }


    public final static String POST_MEETING_URL = "https://gateway.maxhub.com/meeting/api/v1/client/meetings";
    public final static String COMPANY_ID = "412247843";
    public static String postMeeting(String maxhubToken) throws Exception{
        JSONObject bodyMap = new JSONObject();
        bodyMap.put("company_code",COMPANY_ID);
        JSONObject creatorJSON = new JSONObject();
        creatorJSON.put("mobile","13952727261");
        bodyMap.put("creator",creatorJSON);
        bodyMap.put("theme","测试会议");

        long current = DateUtil.current();
        DateTime currentTime = DateUtil.offsetHour(new Date(), 1);
        DateTime endTime = DateUtil.offsetHour(new Date(), 2);
        bodyMap.put("start_time",currentTime.getTime());
        bodyMap.put("end_time",endTime.getTime());

        System.out.println(bodyMap.toString());
        String body = HttpUtil.createPost(RyTask.POST_MEETING_URL).bearerAuth(maxhubToken).contentType("application/json")
                .body(bodyMap.toString() , "application/json")
                .execute().body();
        JSONObject jsonObject = JSONObject.parseObject(body);
        if(jsonObject.getString("code").equals("201")){

        }else{
            throw new Exception("提交Maxhub的预约信息失败；"+body);
        }
        return "";
    }
    // 创建会议室 orgId 1675704383110000642 parentId	"1679316920004026370" spaceCapacity	0 spaceCategoryCode	"SYS001"
}
